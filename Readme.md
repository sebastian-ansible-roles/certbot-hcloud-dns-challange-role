# Readme

This ansible role installs certbot with an auth-hook using hcloud dns service to verify domain ownership. Works for domain specific certificates and wildcard certificates.

## Required vars

```ansible
certbot_hetzner_dns_api_token: "your-hetzner-dns-api-token"
```

## Optional vars

```ansible
cerbot_domain: {{ ansible_fqdn }}
```
